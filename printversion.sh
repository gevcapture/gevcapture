#!/bin/sh

# fallback file containing version number
VERFILE=VERSION

VER=$(git describe --tags --always --dirty 2>/dev/null)
if test -z "$VER" ; then
  if test -f "$VERFILE" ; then
    VER=$(cat "$VERFILE")
  fi
fi
if test -z "$VER" ; then
    VER=unknown
fi

echo "$VER"

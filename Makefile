# Makefile for gevCapture with PYLON version 2.3
# adapted from Makefile for Basler Pylon sample program
.PHONY			: all clean

# the program to build
NAME			:= gevCapture

# includes and library paths:
ARAVIS_INCLUDES		:= -I/usr/local/include/aravis-0.4
ARAVIS_LIBS		:= -L/usr/local/lib
OTHER_INCLUDES		:= $(shell pkg-config --cflags gtk+-3.0)
OTHER_LIBS		:= $(shell pkg-config --libs gtk+-3.0)

# Build tools and flags
CC			:= /usr/bin/gcc
LD			:= /usr/bin/gcc
CCFLAGS		:= -g -O0 -std=c99 -Wall \
			   $(ARAVIS_INCLUDES) $(OTHER_INCLUDES) \
			   -DUSE_GIGE -pthread
LDFLAGS			:= $(ARAVIS_LIBS) $(OTHER_LIBS) \
			   -Wl,-E -lrt -lpthread
LIBS			:= -laravis-0.4

all			: $(NAME)

$(NAME)			: $(NAME).o
	$(LD) $(LDFLAGS) -o $@ $< $(LIBS)

$(NAME).o		: $(NAME).c
	$(CC) $(CCFLAGS) $(CXXFLAGS) -c -o $@ -DVERSION=\"$$(./printversion.sh)\" $<

clean			:
	$(RM) $(NAME).o $(NAME)

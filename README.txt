
gevCapture
==========

You should find some documentation about gevCapture in the Wiki
section associated with the git repository. At the time of writing
one is hosted at

https://gitlab.com/gevcapture/gevcapture/wikis/home

(you can also clone this documentation to your computer via git, and
view it locally using gollum)

For quick reference the documentation as of 2015-03-27 is reproduced
below, but it will not be updated further. Its content is therefore
likely to become more and more inaccurate, and it will be removed
sometime in the future.

__________________________________________________________________
------------------------------------------------------------------

What is it ?
------------

A program for visualising and saving a 8bpp video stream from a
[GigE-vision](gige) compatible camera. Actually simply a bit of glue
connecting the [Aravis]() library (back-end talking to the camera) to
[MPlayer]() (used as front-end) which displays the image.

Currently allows restricting capture and/or saving to a Region Of
Interest, either interactively or via the command line, and selecting
frame-rate, gain and a few other parameters.

[gige] = http://en.wikipedia.org/wiki/GigE_vision
[aravis] = https://live.gnome.org/Aravis
[MPlayer] = http://www.mplayerhq.hu/


Disclaimer (on top of the GPL disclaimer)
-----------------------------------------

This software does not particularly aim at being user friendly. I am
just sharing code I use and hack as I use it. That being said, I use
this program *a lot* and find it very reliable.

I appreciate comments and suggestions, but please understand if I do
not react as you would have hoped I would. In particular, do not
expect much assistance in making this work for you.


Licence
-------

This work is copyright Adrian Daerr, the University Paris Diderot
(2011-2015). Bits borrowed from aravis are copyright Emmanuel Pacaud.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


Dependencies
------------

- [Aravis]() and its dependencies; first commit tested with aravis
  0.1.14, try most recent version (as of this writing aravis is at
  version 0.3.7, and works fine with gevCapture)

- [MPlayer](), (almost) any version should do

- the pthread library


Building
--------

Edit the contents of the Makefile, if needed, then type ./make


Running
-------

gevCapture is designed to be run from the command line.
For a list of options, execute with `--help`.

What gevCapture does is the follwing:

- It launches mplayer and uses aravis to stream video from the named
  camera (or the first camera, if no name is given) to mplayer for
  display.

- It waits for the user to possibly perform some interactive
  adjustments (exposure time, region of interest, ...), and then
  launch the acquisition. From there on gevCapture also streams the
  video to disc, until the requested number of frames have been
  captured or it receives an explicitly quit command.

Output files
------------

Three files with the given file-name as root (default: test) are
created in the current working directory:

1. one for the raw image data (e.g. "test.raw"): 8bpp, row-by-row, all
   frames concatenated

2. one for the timestamps (e.g. "test.stamps"), containing three
   columns: framenumber, frame-time as given by camera, and time when
   frame was received by the computer (with millisecond resolution).
   One line for each frame saved in the raw file.

3. one for some metadata ("test.meta"), such as Region-Of-Interest
   geometry, nominal acquisition frequency etc. This is a text file
   with lines of the form Variable=Value, in a form that can be
   sourced directly e.g. from octave/matlab.

Interactive usage
-----------------

While in interactive mode, the following keys are currently understood
(hit 'h' to see the complete list in the terminal):

    q ................ quit
    h ................ show help
    r ................ toggle Region Of Interest selection
    cursor keys ...... move selection as a whole
    e,s,d,f .......... move upper left corner of selection
    i,j,k,l .......... move lower right corner of selection
    g, SPACE ......... start acquisition

The Region Of Interest can be adjusted using the given keys. Pressing
shift or caps-lock increases the step size. It should be displayed as
a blue overlay.

When hitting <g> or the <SPACE> key, streaming also starts to the file
on disc, limited to the Region Of Interest (by default: maximum size),
which becomes red, and possibly at a lower frame rate (using the
`--capture-interval N` option which will cause N-1 frames in N frames
*not* to be saved). If you wish to record another sequence, you will
need to quit and re-start the program.

Non-interactive parameters
--------------------------

Quite a few things can be adjusted only via command line arguments to
gevCapture. Start gevCapture with the option `--help` to see possible
options.

gevCapture is (currently) programmed to capture periodically, with a
period (counted in frames) given by the `--capture-interval` / `-i` option
(default: 1, i.e. capture every incoming frame). Each capture may
however acquire a whole burst of images (the number of images per
burst is adjusted through `--burst` / `-b`). If for example you want to
acquire 10 images at 20 Hz once every minute, you would invoque
gevCapture as follows:

    gevCapture -f 20 -b 10 -i 120

Here 120 is the number of frames delivered by the camera in one minute.

Some other options call for explanations:

### `--number-of-frames` / `-N`

This is the total number of frames that gevCapture will *save* before
quitting. If this option is zero or not given at all, gevCapture will
never stop capturing until explicitly told to quit.

### Capture size adjustment vs. save region adjustment

The option set

    --x-offset / -x
    --y-offset / -y
    --width / -w
    --height / -h

restricts image acquisition by the image sensor itself to the given
region. In other words pixels outside this area are never even read
out by the camera, much less transferred to the computer. Obviously
you will only be able to see the selected region on screen and save at
most these pixels. On the other hand the bandwidth usage and
processing load is reduced, and on most cameras the achieveable
frame-rate is higher when only part of the sensor is read out.

By contrast, the option set

    --save-x-offset / -1
    --save-y-offset / -2
    --save-width / -3
    --save-height / -4

only restricts *saving* to the given region. The display shows the
whole picture (or whatever has been selected using the first set of
options). This region that may also be set interactively after
pressing the 'r' key (press 'h' for help on how to change the
region-of-interest geometry).

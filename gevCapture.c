/*
 *  gevCapture
 *
 * A program for visualising and saving a 8bpp video stream from a
 * compatible camera, using aravis and mplayer.
 *
 * This work is copyright Adrian Daerr, the University Paris Diderot
 * (2011-2015). Bits borrowed from aravis are copyright Emmanuel
 * Pacaud.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */

// following needed to have mkdtemp() in stdlib.h
#define _DEFAULT_SOURCE
// following needed to have fdopen() in stdio.h
#define _XOPEN_SOURCE
// following must to be >=199309L to have clock_gettime() in time.h
#define _POSIX_C_SOURCE 199309L

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>
#include <termios.h>
#include <arpa/inet.h>
#include <signal.h>
#include <time.h>
#include <arv.h>

// number of buffers in ring
#define nBuffers 100

// max display updates per second
const double display_fps_default = 5;
// max display updates per second when recording
const double display_fps_saving = .2;

#ifndef _POSIX_MONOTONIC_CLOCK
#warning This system does not implement
#warning CLOCK_MONOTONIC, which we use,
#warning so compilation is likely to abort.
#warning replace by CLOCK_REALTIME ?
#warning (also see clock_getres(2))
#endif

/* macros are evil, but these ones are just too tempting; they make
   checking return codes of system calls easy and pretty (did someone
   juste say 'perl' ?), at least as long as we are ready to die for
   every cause...
*/
/* die with message */
#pragma GCC diagnostic ignored "-Wunused-value"
#define DIE(x) (perror(x), exit(EXIT_FAILURE), 1)
#pragma GCC diagnostic pop
/* die with message if preceeding command returns -1; "not -1 or die" */
#define NM1_OR_DIE(x) != -1 || DIE(x)
/* die with message if preceeding cmd returns non-zero int: "zero or die" */
#define Z_OR_DIE(x) == 0 || DIE(x)
/* die with message if preceeding command returns NULL: "non-null or die" */
#define NN_OR_DIE(x) != NULL || DIE(x)
/* complain, but no seppuku */
#define COMPLAIN(x) (perror(x), 1)

// used for args, filenames, ...
#define BUFSIZE 256
#define MPLAYER_MAXARGC 40

/* structure defining region of interest */
struct ROI {
  gboolean valid;
  int x, y, width, height;
};

typedef struct {
  GMainLoop *main_loop;
  int buffer_count;
  int saved_count;
} ApplicationData;


/*
 * globally accessible variables
 */

// global variables
static char *arv_option_camera_name = NULL;
static char *arv_option_debug_domains = NULL;
static char *arv_option_trigger = NULL;
static double arv_option_software_trigger_timeout = -1;
static double arv_option_frequency = -1.0;
static int arv_option_numframes = 0;
static int arv_option_capture_interval = 1;
static int arv_option_burst_size = 1;
static int arv_option_width = -1;
static int arv_option_height = -1;
static int arv_option_horizontal_binning = -1;
static int arv_option_vertical_binning = -1;
static double arv_option_exposure_time_us = -1;
static int arv_option_gain = -1;
static gboolean arv_option_auto_socket_buffer = false;
static gboolean arv_option_no_packet_resend = false;
static unsigned int arv_option_packet_timeout = 20;
static unsigned int arv_option_frame_retention = 100;
static gboolean arv_option_window_fit = false;
static int arv_option_save_width = -1;
static int arv_option_save_height = -1;
static int arv_option_save_x_offset = -1;
static int arv_option_save_y_offset = -1;
static int arv_option_x_offset = -1;
static int arv_option_y_offset = -1;
static char *arv_option_file_root = NULL;

static const GOptionEntry arv_option_entries[] =
{
  {
    "file-name", 'n', 0, G_OPTION_ARG_STRING,
    &arv_option_file_root,      "Output file name root", "s"
  },
  {
    "camera-name", 'c', 0, G_OPTION_ARG_STRING,
    &arv_option_camera_name,    "Camera name", "s"
  },
  {
    "x-offset", 'x', 0, G_OPTION_ARG_INT,
    &arv_option_x_offset,      "X-offset", "N"
  },
  {
    "y-offset", 'y', 0, G_OPTION_ARG_INT,
    &arv_option_y_offset,      "Y-offset", "N"
  },
  {
    "width", 'w', 0, G_OPTION_ARG_INT,
    &arv_option_width,      "Width", "N"
  },
  {
    "height", 'h', 0, G_OPTION_ARG_INT,
    &arv_option_height,       "Height", "N"
  },
  {
    "h-binning", '\0', 0, G_OPTION_ARG_INT,
    &arv_option_horizontal_binning,    "Horizontal binning", "N"
  },
  {
    "v-binning", '\0', 0, G_OPTION_ARG_INT,
    &arv_option_vertical_binning,     "Vertical binning", "N"
  },
  {
    "save-x-offset", '1', 0, G_OPTION_ARG_INT,
    &arv_option_save_x_offset,    "Saved ROI x-offset", "N"
  },
  {
    "save-y-offset", '2', 0, G_OPTION_ARG_INT,
    &arv_option_save_y_offset,    "Saved ROI y-offset", "N"
  },
  {
    "save-width", '3', 0, G_OPTION_ARG_INT,
    &arv_option_save_width,      "Saved ROI width", "N"
  },
  {
    "save-height", '4', 0, G_OPTION_ARG_INT,
    &arv_option_save_height,     "Saved ROI height", "N"
  },
  {
    "frequency", 'f', 0, G_OPTION_ARG_DOUBLE,
    &arv_option_frequency,      "Acquisition frequency", "f"
  },
  {
    "number-of-frames", 'N', 0, G_OPTION_ARG_INT,
    &arv_option_numframes, "Total number of frames", "N"
  },
  {
    "capture-interval", 'i', 0, G_OPTION_ARG_INT,
    &arv_option_capture_interval,    "Period (in frames) of captures (frames or bursts)", "N"
  },
  {
    "burst", 'b', 0, G_OPTION_ARG_INT,
    &arv_option_burst_size,    "Acquire N frames (at acqu freq) at every capture", "N"
  },
  {
    "window-fit", 'r', 0, G_OPTION_ARG_NONE,
    &arv_option_window_fit,      "Rescale window so it fits on screen", NULL
  },
  {
    "trigger", 't', 0, G_OPTION_ARG_STRING,
    &arv_option_trigger,      "Trigger source (Software|Line 1|..)", "s"
  },
  {
    "software-trigger-timeout", 'o', 0, G_OPTION_ARG_DOUBLE,
    &arv_option_software_trigger_timeout,    "Software trigger period", "T"
  },
  {
    "exposure", 'e', 0, G_OPTION_ARG_DOUBLE,
    &arv_option_exposure_time_us,     "Exposure time (us)", "T"
  },
  {
    "gain", 'g', 0, G_OPTION_ARG_INT,
    &arv_option_gain,       "Gain (dB)", "N"
  },
  {
    "auto", 'a', 0, G_OPTION_ARG_NONE,
    &arv_option_auto_socket_buffer,    "Auto socket buffer size", "N"
  },
  {
    "no-packet-resend", 'r', 0, G_OPTION_ARG_NONE,
    &arv_option_no_packet_resend,    "No packet resend", NULL
  },
  {
    "packet-timeout", 'p', 0, G_OPTION_ARG_INT,
    &arv_option_packet_timeout,     "Packet timeout (ms)", "N"
  },
  {
    "frame-retention", 'm', 0, G_OPTION_ARG_INT,
    &arv_option_frame_retention,     "Frame retention (ms)", "N"
  },
  {
    "debug", 'd', 0, G_OPTION_ARG_STRING,
    &arv_option_debug_domains,     "Debug domains", "s"
  },
  { NULL }
};

ArvCamera *camera;
ArvStream *stream;

FILE *imageFile;
FILE *stampFile;
FILE *metaFile;

struct timespec clock_res, clock_last, clock_now;
guint software_trigger_source;

struct ROI imageROI, display;
int exposureVal = -1;

struct timespec clock_origin;

/* polled by some threads to know when to quit
   protected by end_of_world_mutex */
gboolean end_of_world = false;

/* save ? (no | yes | start saving ASAP (and become yes))
   protected by saving_state_mutex */
typedef enum { SAVE_NO, SAVE_YES, SAVE_START } saving_t;
saving_t saving_state = SAVE_NO;

/* indicates that overlay is out of date
   protected by update_overlay_mutex */
gboolean update_overlay = false;

/* The Region Of Interest is adjusted in the UI thread and
   used by the overlay (bmovl) thread to generate a mask;
   it is protected by update_overlay_mutex */
struct ROI subRegion;

/* sensor region for differential acquisition
   protected by the update_overlay_mutex */
struct ROI sensor;

/* video display related stuff
   protected by pixel_data_mutex */
const uint8_t * pixel_data = NULL;

/* This is a circular queue which is used to store the buffers to be
   saved. Filled by the aravis capture thread (running new_buffer_cb)
   and the processed by the frame-to-disk-saver thread. The processed
   buffers are then pushed back by the capture thread.
   protected by the buffer_save_mutex */
#define BuffQueueLen (nBuffers+1)
ArvBuffer *buffer_queue[BuffQueueLen];
int buffer_queue_head = 0;
int buffer_queue_tail = 0;

/* continuous or triggered acquisition ? */
gboolean continuous_acquisition;

/* mutexes and condition variables */

pthread_mutex_t end_of_world_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t saving_state_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t update_overlay_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t pixel_data_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t buffer_save_mutex = PTHREAD_MUTEX_INITIALIZER;

pthread_cond_t videostream_update = PTHREAD_COND_INITIALIZER;
pthread_cond_t overlay_update = PTHREAD_COND_INITIALIZER;
pthread_cond_t buffer_save_new = PTHREAD_COND_INITIALIZER;

/*
 *  functions
 */

void usage(char* execname) {
  fprintf(stderr,"usage:\n%s [options]", execname);
}

void end_world() {
  pthread_mutex_lock(&end_of_world_mutex);
  end_of_world = true;
  pthread_mutex_unlock(&end_of_world_mutex);
  //fprintf(stderr,"gevCapture's world is about to end...\n");
}

gboolean open_output_files(const char* root) {
  const char* imageExt = ".raw";
  const char* stampExt = ".stamps";
  const char* metaExt = ".meta";
  struct stat *fileinfo = (struct stat *) g_malloc(sizeof(struct stat));

  if (root == NULL) root = "test";

  // calculate maximum filename size
  size_t s = strlen(imageExt);
  if (strlen(stampExt) > s) s = strlen(stampExt);
  if (strlen(metaExt) > s) s = strlen(metaExt);
  s += strlen(root) + 1;// add filename root length and room for '\0'
  char* filename = (char*) g_malloc(s);// allocate room for filename

  // build file names and open all three output files
  (snprintf(filename, s, "%s%s", root, imageExt) < (int)s)
    || DIE("snprintf(imageExt)");
  // if image file exists (and has non-zero size) then abort,
  // as safeguard against overwriting a previous recording
  if (access(filename, F_OK) == 0) {
    stat(filename, fileinfo);
    if (fileinfo->st_size > 0) {
      fprintf(stderr, "Output file %s already exists, aborting.\n", filename);
      g_free(filename);
      g_free(fileinfo);
      exit(EXIT_FAILURE);
    }
  }
  (imageFile = fopen(filename, "w")) NN_OR_DIE("fopen(imageFile)");
  (snprintf(filename, s, "%s%s", root, stampExt) < (int)s)
    || DIE("snprintf(stampExt)");
  (stampFile = fopen(filename, "w")) NN_OR_DIE("fopen(stampFile)");
  (snprintf(filename, s, "%s%s", root, metaExt) < (int)s)
    || DIE("snprintf(metaExt)");
  (metaFile = fopen(filename, "w")) NN_OR_DIE("fopen(metaFile)");

  g_free(filename);
  g_free(fileinfo);

  return true;
}

static gboolean
emit_software_trigger (void *abstract_data)
{
  ArvCamera *camera = (ArvCamera *) abstract_data;

  fprintf(stderr, "emitting software trigger...\n");
  arv_camera_software_trigger (camera);

  return true;
}

/* Toggle saving state. When starting to save, meta-information about
   the current acquisition is dumped into a .meta file. */
gboolean start_stop_saving() {
  saving_t saving;

  pthread_mutex_lock(&saving_state_mutex);
  if (saving_state == SAVE_NO)
    saving = saving_state = SAVE_START;
  else
    saving = saving_state = SAVE_NO;
  pthread_mutex_unlock(&saving_state_mutex);

  if (saving == SAVE_NO) return false;

  fprintf(metaFile,"subRegionX=%d\n",subRegion.x);
  fprintf(metaFile,"subRegionY=%d\n",subRegion.y);
  fprintf(metaFile,"subRegionWidth=%d\n",subRegion.width);
  fprintf(metaFile,"subRegionHeight=%d\n",subRegion.height);
  fprintf(metaFile,"captureCameraName=\"%s\"\n",arv_camera_get_model_name (camera));
  fprintf(metaFile,"captureFrequency=%f\n",arv_camera_get_frame_rate (camera));
  fprintf(metaFile,"captureInterval=%d\n",arv_option_capture_interval);
  fprintf(metaFile,"captureBurstSize=%d\n",arv_option_burst_size);
  fprintf(metaFile,"captureExposureTime=%f\n",arv_option_exposure_time_us);
  fprintf(metaFile,"captureProg=\"gevCapture %s\"\n",VERSION);

  fclose(metaFile);
  metaFile = NULL;

  fprintf(stderr,"\n saving stream !\n");

  return true;
}

struct ROI start_camera() {

  if (arv_option_camera_name == NULL)
    g_print ("Looking for the first available camera\n");
  else
    g_print ("Looking for camera '%s'\n", arv_option_camera_name);

  /* find camera */
  camera = arv_camera_new (arv_option_camera_name);
  camera NN_OR_DIE("camera_new");

  gint payload;
  gint x, y, width, height;
  gint dx, dy;
  double exposure;
  int gain;

  arv_camera_get_sensor_size(camera, &width, &height);
  g_print ("Sensor size: %i x %i\n", width, height);
  if (arv_option_x_offset == -1 || arv_option_width == -1) {
    arv_option_x_offset = 0;
    arv_option_width = width;
  }
  if (arv_option_y_offset == -1 || arv_option_height == -1) {
    arv_option_y_offset = 0;
    arv_option_height = height;
  }

  arv_camera_set_region (camera, arv_option_x_offset, arv_option_y_offset, arv_option_width, arv_option_height);
  arv_camera_set_binning (camera, arv_option_horizontal_binning, arv_option_vertical_binning);
  arv_camera_set_exposure_time (camera, arv_option_exposure_time_us);
  arv_camera_set_gain (camera, arv_option_gain);

  arv_camera_get_region (camera, &x, &y, &width, &height);
  arv_camera_get_binning (camera, &dx, &dy);
  exposure = arv_camera_get_exposure_time (camera);
  payload = arv_camera_get_payload (camera);
  gain = arv_camera_get_gain (camera);

  printf ("vendor name         = %s\n", arv_camera_get_vendor_name (camera));
  printf ("model name          = %s\n", arv_camera_get_model_name (camera));
  printf ("device id           = %s\n", arv_camera_get_device_id (camera));
  printf ("image width         = %d\n", width);
  printf ("image height        = %d\n", height);
  printf ("horizontal binning  = %d\n", dx);
  printf ("vertical binning    = %d\n", dy);
  printf ("exposure            = %g µs\n", exposure);
  printf ("gain                = %d dB\n", gain);

  /* open stream on camera */
  stream = arv_camera_create_stream (camera, NULL, NULL);
  stream NN_OR_DIE("create_stream");

  if (ARV_IS_GV_STREAM (stream)) {
    if (arv_option_auto_socket_buffer)
      g_object_set (stream,
                    "socket-buffer", ARV_GV_STREAM_SOCKET_BUFFER_AUTO,
                    "socket-buffer-size", 0,
                    NULL);
    if (arv_option_no_packet_resend)
      g_object_set (stream,
                    "packet-resend", ARV_GV_STREAM_PACKET_RESEND_NEVER,
                    NULL);
    g_object_set (stream,
                  "packet-timeout", (unsigned) arv_option_packet_timeout * 1000,
                  "frame-retention", (unsigned) arv_option_frame_retention * 1000,
                  NULL);
  }

  for (int i = 0; i < nBuffers; i++)
    arv_stream_push_buffer (stream, arv_buffer_new (payload, NULL));

  if (arv_option_frequency > 0.0)
    arv_camera_set_frame_rate (camera, arv_option_frequency);

  /* Camera can be configured either as continuously acquiring, or as
     capturing single frames. In the latter case, the trigger source
     can be either Software (time-out via the
     arv_option_software_trigger_timeout parameter, or <space> bar),
     or hardware (depending on camera model e.g. "Line 1" for the
     corresponding input pin. */

  if (continuous_acquisition)
    arv_camera_set_acquisition_mode (camera, ARV_ACQUISITION_MODE_CONTINUOUS);
  else
    arv_camera_set_acquisition_mode (camera, ARV_ACQUISITION_MODE_SINGLE_FRAME);

  if (arv_option_trigger == NULL) {
    ArvDevice *device = arv_camera_get_device (camera);
    // TriggerSelector -> AcquisitionStart
    arv_device_set_string_feature_value (device, "TriggerSelector", "AcquisitionStart");
    arv_device_set_string_feature_value (device, "TriggerMode", "Off");
  } else {
    arv_camera_set_trigger (camera, arv_option_trigger);
  }

  // retrieve and return real image dimensions
  struct ROI imageROI;
  arv_camera_get_region (camera, &imageROI.x, &imageROI.y, &imageROI.width, &imageROI.height);
  imageROI.valid = true;

  return imageROI;
}

  /* Clean up */
void stop_camera() {
  guint64 n_completed_buffers;
  guint64 n_failures;
  guint64 n_underruns;

  arv_stream_get_statistics (stream,
                             &n_completed_buffers,
                             &n_failures,
                             &n_underruns);

  printf("Completed buffers = %Lu\n", (unsigned long long) n_completed_buffers);
  printf("Failures          = %Lu\n", (unsigned long long) n_failures);
  printf("Underruns         = %Lu\n", (unsigned long long) n_underruns);

  arv_camera_stop_acquisition (camera);
}

  /* time difference in milliseconds */
long clock_diff_millis(struct timespec t1, struct timespec t2) {
  long d;
  d = ((long)t1.tv_sec - (long)t2.tv_sec)*1000
      + (t1.tv_nsec - t2.tv_nsec)/1000000;
  return d;
}

  /* return time from timespec-structure in milliseconds */
long clock_millis(struct timespec t) {
  return ((long)t.tv_sec)*1000 + t.tv_nsec/1000000;
}


/* Called from the aravis thread as new buffers become available from camera */
static void
new_buffer_cb (ArvStream *stream, ApplicationData *data)
{
  ArvBuffer *buffer;
  guint32 frame_id;
  double display_fps = display_fps_default;
  gboolean push_back_at_end = true; // push buffer back at end ?

  // remember previous frame id to detect overflow
  static guint32 old_frame_id=0;
  // frame_id that does not wrap around (until guint32 itself overflows)
  static guint32 unwrapped_frame_id=0;
  // wrapping modulus
  const guint32 frame_id_modulus = 1<<16;
  // next frame id where we start to capture
  static guint32 next_save_frame_id;
  // number of frames still to be captured in current burst
  static guint32 burst_remaining;

  buffer = arv_stream_try_pop_buffer (stream);

  if (buffer == NULL) {
    fprintf(stderr,"0");
    return;
  }

  if (arv_buffer_get_status(buffer) != ARV_BUFFER_STATUS_SUCCESS) {
    fprintf(stderr,"X");
    arv_stream_push_buffer (stream, buffer);
    return;
  }

  data->buffer_count++;

  // unwrap running frame number
  frame_id = arv_buffer_get_frame_id(buffer); //data->buffer_count
  if (frame_id < old_frame_id)
    unwrapped_frame_id += (frame_id + frame_id_modulus) - old_frame_id;
  else
    unwrapped_frame_id += frame_id - old_frame_id;
  old_frame_id = frame_id;

  clock_gettime(CLOCK_MONOTONIC, &clock_now);

  // Save timestamps and image:
  // check if we are supposed to save
  saving_t saving;

  pthread_mutex_lock(&saving_state_mutex);
  saving = saving_state;
  if (saving == SAVE_START) saving_state = SAVE_YES;
  pthread_mutex_unlock(&saving_state_mutex);

  if (saving) {
    if (saving == SAVE_START) {
      unwrapped_frame_id = 0;
      next_save_frame_id = 0;
    }

    // start capturing new burst of frames ?
    if (burst_remaining == 0 && unwrapped_frame_id >= next_save_frame_id)
      burst_remaining = arv_option_burst_size;

    if (burst_remaining > 0) { // save frame ! (and stamp data)

      // write meta-data
      fprintf(stampFile, "%d\t%ld\t%ld\n",
	      unwrapped_frame_id,              // running frame number, guint32
	      arv_buffer_get_timestamp(buffer),// camera time (ns), guint64
	      clock_millis(clock_now));        // computer time (ms)

      pthread_mutex_lock(&buffer_save_mutex);

      // to avoid lock-up with the buffer_save_thread, we only
      // push the current buffer for saving if it is not the last
      // we have. Otherwise we need to start dropping frames
      // unfortunately...

      // how many buffers are currently waiting to be saved ?
      int N = (buffer_queue_head - buffer_queue_tail + BuffQueueLen) % BuffQueueLen;

      if (N < nBuffers-1) {
	push_back_at_end = false; // this frame will be pushed back only
	// later, once it has been saved, so don't do it at the end of
	// this cb.

	// add the current buffer
	buffer_queue[buffer_queue_head++] = buffer;
	if (buffer_queue_head == BuffQueueLen)
	  buffer_queue_head = 0;
	// wake the frame_saver_task
	pthread_cond_signal(&buffer_save_new);
	//fprintf(stderr,"s");
	pthread_mutex_unlock(&buffer_save_mutex);

	// check if enough frames were acquired
	if ((arv_option_numframes > 0) &&
	    (++(data->saved_count) >= arv_option_numframes))
	  end_world();
	else if (!continuous_acquisition)// re-arm camera for next trigger
	  arv_camera_start_acquisition (camera);

	// finally calculate when the next acquisition burst starts
	if (--burst_remaining <= 0) {
	  next_save_frame_id += arv_option_capture_interval;
	  // if we missed a lot of frames do a slighly more convoluted
	  // calculation, so that the next bursts starts at a multiple
	  // of capture_interval from the previous
	  if (unwrapped_frame_id >= next_save_frame_id)
	    next_save_frame_id = unwrapped_frame_id - ((unwrapped_frame_id - next_save_frame_id) % arv_option_capture_interval) + arv_option_capture_interval;
	}
      } else/* N >= nBuffers-1 */ {// frame dropped !
	push_back_at_end = true;
	fprintf(stderr,"!");
	pthread_cond_signal(&buffer_save_new);
	pthread_mutex_unlock(&buffer_save_mutex);
      }
    } else {/* save mode, but this frame not in burst */
      //fprintf(stderr,".");
    }
    display_fps = display_fps_saving;
  } else {/* not saving */
    //fprintf(stderr,".");
  }

  // check time since last update and request one if needed
  if (clock_diff_millis(clock_now, clock_last)*display_fps > 1000) {
    // Get the pointer to the image buffer
    const uint8_t *pImageBuffer = (uint8_t *) arv_buffer_get_data(buffer,NULL);
    pthread_mutex_lock(&pixel_data_mutex);
    pixel_data = pImageBuffer;
    pthread_mutex_unlock(&pixel_data_mutex);
    pthread_cond_signal(&videostream_update);
    clock_last = clock_now;
  }

  if (push_back_at_end) arv_stream_push_buffer (stream, buffer);
}

void *buffer_save_task(void* unused) {
  int N = 0;
  uint8_t *pixelData;
  gboolean quit;
  static struct ROI roi; // initialised to zero

  while (1) {

    pthread_mutex_lock(&buffer_save_mutex);
    if (N > 0) {// means one buffer was saved, push it back to aravis
      arv_stream_push_buffer (stream, buffer_queue[buffer_queue_tail]);
      buffer_queue_tail = (buffer_queue_tail + 1) % BuffQueueLen;
    }
    while (!quit && buffer_queue_head == buffer_queue_tail) {
      pthread_cond_wait(&buffer_save_new, &buffer_save_mutex);
      // where we awakened to quit ?
      pthread_mutex_lock(&end_of_world_mutex);
      quit = end_of_world;
      pthread_mutex_unlock(&end_of_world_mutex);
      if (quit)
        fprintf(stderr, "buffer_save_task: will quit\n");
    }

    // do we have some buffers to save ?
    N = (buffer_queue_head - buffer_queue_tail + BuffQueueLen) % BuffQueueLen;
    //fprintf(stderr,"%d", N);

    if (N != 0)
      // Get the pointer to the image buffer before releasing the lock
      pixelData = (uint8_t *)
	arv_buffer_get_data(buffer_queue[buffer_queue_tail], NULL);

    pthread_mutex_unlock(&buffer_save_mutex);

    if (N == 0) {
      if (quit)
	break;
      else
	continue;
    }

    // print warning if buffer queue more than half filled
    if (2*N > nBuffers) fprintf(stderr,"<save:%d>", N);

    // check whether ROI has already been initialised
    if (roi.height == 0) {
        pthread_mutex_lock(&update_overlay_mutex);
	roi = subRegion;
        pthread_mutex_unlock(&update_overlay_mutex);
    }

    if (imageROI.width == roi.width) {
      fwrite(pixelData+roi.y*roi.width, 1, roi.width*roi.height, imageFile);
    } else {
      for (int y=roi.y; y<roi.y+roi.height; y++) {
	size_t offset = y*imageROI.width + roi.x;
	fwrite(pixelData+offset, 1, roi.width, imageFile);
      }
    }

  }
  // close image file
  fclose(imageFile);
  imageFile = NULL;

  fprintf(stderr, "buffer_save_task: quitting as requested\n");
  pthread_exit(NULL);
}

static gboolean
periodic_task_cb (void *abstract_data)
{
  ApplicationData *data = (ApplicationData *) abstract_data;
  gboolean quit = false;

  // check if we should quit
  pthread_mutex_lock(&end_of_world_mutex);
  quit = end_of_world;
  pthread_mutex_unlock(&end_of_world_mutex);
  if (quit)  {
    g_main_loop_quit (data->main_loop);
    return false;
  }

  return true;
}

  /* signal handler */
void sighandler(int signum) {
  fprintf(stderr,"received signal %d ... ", signum);
  switch (signum) {
  case SIGQUIT:
    fprintf(stderr,"quitting\n");
    end_world();
    break;
  case SIGPIPE:
  default:
    fprintf(stderr,"ignoring\n");
  }
}

  /* output dummy frame */
void dummyframe(FILE *stream) {
  for (int y=0; y<display.height; y++) {
    for (int x=0; x<display.width; x++) {
      if (fputc(x+y, stream) == EOF) return;
    }
  }
  fflush(stream) Z_OR_DIE("fflush");
}

  /* cut off trailing whitespace and newline characters */
void chomp(char *s)
{
  int i;
  char c;
  i = strlen(s);
  do {
    *(s+i) = '\0';
    i--;
    c = *(s+i);
  } while (i >= 0 && (c == ' ' || c == '\n' || c == '\t' || c == '\r'));
}

/* Show short help on stderr */
void show_help() {
  fprintf(stderr,"Available key bindings:\n");
  fprintf(stderr,"q  ... quit; h ... show help\n");
  fprintf(stderr,"r ................ toggle Region Of Interest selection\n");
  fprintf(stderr,"m ................ toggle Movement Sensor selection\n");
  fprintf(stderr,"cursor keys ...... move selection as a whole\n");
  fprintf(stderr,"e,s,d,f .......... move upper left corner of selection\n");
  fprintf(stderr,"i,j,k,l .......... move lower right corner of selection\n");
  fprintf(stderr,"+,- .............. change exposure time\n");
  fprintf(stderr,"n ................ enter 'one in N frames' save rate\n");
  fprintf(stderr,"g, SPACE ......... start acquisition\n");
}

  /* process keypress event */
void keypress (char *s)
{
  static gboolean saving = false;
  static gboolean subRegion_adjust = false;

  if (strcmp(s,"q") == 0 ||
      strcmp(s,"Q") == 0 ||
      strcmp(s,"CLOSE_WIN") == 0 ||
      strcmp(s,"ESC") == 0) {// quit
    fprintf(stderr, "quit...\n");
    end_world();
  } else if (strcmp(s,"h") == 0 || strcmp(s,"H") == 0) {// help
    show_help();
  } else if (strcmp(s,"g") == 0 ||
             strcmp(s,"G") == 0 ||
             strcmp(s,"SPACE") == 0) {// save-to-disk
    if (continuous_acquisition) {
      saving = start_stop_saving();
      if (!saving) // we just stopped saving
        end_world();
      else { // for visual feedback
        pthread_mutex_lock(&update_overlay_mutex);
        update_overlay = true;
        pthread_mutex_unlock(&update_overlay_mutex);
      }
    } else {// triggered acquisition
      emit_software_trigger(camera);
    }
  } else if (strcmp(s,"+") == 0 || strcmp(s,"-") == 0) {// exposure
    double exposureVal = arv_camera_get_exposure_time (camera);
    double expmax, expmin;
    arv_camera_get_exposure_time_bounds(camera, &expmin, &expmax);
    if (strcmp(s,"+") == 0)
      exposureVal += 1e-3;
    else
      exposureVal -= 1e-3;
    if (exposureVal >= expmin && exposureVal <= expmax)
      arv_camera_set_exposure_time (camera, exposureVal);
    fprintf(stderr, "exposureVal = %f us\n", exposureVal);
  } else if (strcmp(s,"r") == 0 || strcmp(s,"R") == 0) {// toggle ROI
    pthread_mutex_lock(&update_overlay_mutex);
    if (!subRegion.valid) {
      subRegion.valid = true;
      subRegion_adjust = true;
      update_overlay = true;
    } else {
      if (subRegion_adjust) {
        subRegion.valid = false;
        update_overlay = true;
      } else
        subRegion_adjust = true;
    }
    pthread_mutex_unlock(&update_overlay_mutex);
  } else if (0) {//strcmp(s,"t") == 0 || strcmp(s,"T") == 0) {// toggle Sensor
    pthread_mutex_lock(&update_overlay_mutex);
    if (!sensor.valid) {
      sensor.valid = true;
      subRegion_adjust = false;// this means sensor becomes adjustable
      update_overlay = true;
    } else {
      if (subRegion_adjust) {
        subRegion_adjust = false;
      } else {
        sensor.valid = false;
        update_overlay = true;
      }
    }
    pthread_mutex_unlock(&update_overlay_mutex);
  } else if (!saving && subRegion.valid && subRegion_adjust) {
    pthread_mutex_lock(&update_overlay_mutex);
    if (strcmp(s,"e") == 0 || strcmp(s,"E") == 0) {// NW corner up
      int incr = 1;
      if (strcmp(s,"E") == 0) incr = 20;
      if (subRegion.y > 0) {
        if (subRegion.y - incr < 0) incr = subRegion.y;
        subRegion.y -= incr;
        subRegion.height += incr;
        update_overlay = true;
      }
    } else if (strcmp(s,"d") == 0 || strcmp(s,"D") == 0) {// NW corner down
      int incr = 1;
      if (strcmp(s,"D") == 0) incr = 20;
      if (subRegion.y < imageROI.height - 1) {
        if (subRegion.y + incr >= imageROI.height)
          incr = imageROI.height - 1 - subRegion.y;
        subRegion.y += incr;
        subRegion.height -= incr;
        if (subRegion.height <= 0) subRegion.height = 1;
        update_overlay = true;
      }
    } else if (strcmp(s,"s") == 0 || strcmp(s,"S") == 0) {// NW corner left
      int incr = 1;
      if (strcmp(s,"S") == 0) incr = 20;
      if (subRegion.x > 0) {
        if (subRegion.x - incr < 0) incr = subRegion.x;
        subRegion.x -= incr;
        subRegion.width += incr;
        update_overlay = true;
      }
    } else if (strcmp(s,"f") == 0 || strcmp(s,"F") == 0) {// NW corner right
      int incr = 1;
      if (strcmp(s,"F") == 0) incr = 20;
      if (subRegion.x < imageROI.width - 1) {
        if (subRegion.x + incr >= imageROI.width)
          incr = imageROI.width - 1 - subRegion.x;
        subRegion.x += incr;
        subRegion.width -= incr;
        if (subRegion.width <= 0) subRegion.width = 1;
        update_overlay = true;
      }
    } else if (strcmp(s,"i") == 0 || strcmp(s,"I") == 0) {// SE corner up
      int incr = 1;
      if (strcmp(s,"I") == 0) incr = 20;
      if (subRegion.y + subRegion.height > 0) {
        if (subRegion.height > incr)
          subRegion.height -= incr;
        else {
          subRegion.y -= incr - (subRegion.height - 1);
          subRegion.height = 1;
          if (subRegion.y < 0) subRegion.y = 0;
        }
        update_overlay = true;
      }
    } else if (strcmp(s,"k") == 0 || strcmp(s,"K") == 0) {// SE corner down
      int incr = 1;
      if (strcmp(s,"K") == 0) incr = 20;
      if (subRegion.y + subRegion.height < imageROI.height) {
        if (subRegion.y + subRegion.height + incr > imageROI.height)
          incr = imageROI.height - (subRegion.y + subRegion.height);
        subRegion.height += incr;
        update_overlay = true;
      }
    } else if (strcmp(s,"j") == 0 || strcmp(s,"J") == 0) {// SE corner left
      int incr = 1;
      if (strcmp(s,"J") == 0) incr = 20;
      if (subRegion.x + subRegion.width > 0) {
        if (subRegion.width > incr)
          subRegion.width -= incr;
        else {
          subRegion.x -= incr - (subRegion.width - 1);
          subRegion.width = 1;
          if (subRegion.x < 0) subRegion.x = 0;
        }
        update_overlay = true;
      }
    } else if (strcmp(s,"l") == 0 || strcmp(s,"L") == 0) {// SE corner right
      int incr = 1;
      if (strcmp(s,"L") == 0) incr = 20;
      if (subRegion.x + subRegion.width < imageROI.width ) {
        if (subRegion.x + subRegion.width + incr > imageROI.width)
          incr =  imageROI.width - (subRegion.x + subRegion.width);
        subRegion.width += incr;
        update_overlay = true;
      }
    } else if (strcmp(s,"UP") == 0) {// Region up
      if (subRegion.y > 0) {
        subRegion.y -= 1;
        update_overlay = true;
      }
    } else if (strcmp(s,"DOWN") == 0) {// Region down
      if (subRegion.y + subRegion.height < imageROI.height) {
        subRegion.y += 1;
        update_overlay = true;
      }
    } else if (strcmp(s,"LEFT") == 0) {// Region left
      if (subRegion.x > 0) {
        subRegion.x -= 1;
        update_overlay = true;
      }
    } else if (strcmp(s,"RIGHT") == 0) {// Region right
      if (subRegion.x + subRegion.width < imageROI.width ) {
        subRegion.x += 1;
        update_overlay = true;
      }
    }
    pthread_mutex_unlock(&update_overlay_mutex);
  } else if (!saving && sensor.valid && !subRegion_adjust) {
    fprintf(stderr,"add sensor adjustment interface!\n");
  } else { // undigested key
    fprintf(stderr, "<%s>", s);
  }
}

/* user interface listener, parsing mplayer's stderr, forked from main */
void *err_listener(void* input)
{
  const char *nobind1 = "No bind found for key '";
  const char *nobind2 = "'.";
  char line[BUFSIZE];
  gboolean quit = false;
  while (!quit && fgets(line, sizeof(line), (FILE*)input) != NULL) {
    chomp(line);
    if (strlen(line) > strlen(nobind2) &&
        strncmp(line, nobind1, strlen(nobind1)) == 0 &&
        strncmp(&line[strlen(line)-strlen(nobind2)],
                nobind2, strlen(nobind2)) == 0) {
      line[strlen(line)-strlen(nobind2)] = '\0';
      keypress(line+strlen(nobind1));
    } else {
      fprintf(stderr, "err_listener: %s\n", line);
    }
    // shall we quit ?
    pthread_mutex_lock(&end_of_world_mutex);
    quit = end_of_world;
    pthread_mutex_unlock(&end_of_world_mutex);
  }
  fprintf(stderr, "err_listener: quitting as requested\n");
  pthread_exit(NULL);
}

  /* second user interface listener, on standard in, forked from main */
void *stdin_listener(void* input)
{
  // quick'n'dirty translation tables for some escape sequences
  const char *mapkeys[] = { "\e[A", "\e[B", "\e[C", "\e[D", NULL };
  const char *mapvals[] = { "UP", "DOWN", "RIGHT", "LEFT", NULL};
  int c, offset, i;
  char s[BUFSIZE];
  offset = 0;
  fprintf(stderr,"+");
  while ((c = fgetc((FILE*)input)) != EOF) {
    s[offset++] = (char)c;
    if (s[0] == '\e' && offset < 3) continue;
    s[offset] = '\0';
    if (offset > 1) {
      for (i=0; mapkeys[i] != NULL; i++) {
        if (strncmp(s, mapkeys[i], strlen(mapkeys[i])) == 0) break;
      }
      if (mapkeys[i] != NULL) strcpy(s, mapvals[i]);
    }
    keypress(s);
    offset = 0;
  }
  fprintf(stderr, "stdin_listener: read EOF, exiting\n");
  return NULL;
}

void generate_mask(u_int32_t mask[], struct ROI rect, const gboolean saving) {
  u_int32_t sensor_pix = htonl(0x00FF0080);
  u_int32_t none_pix = htonl(0x00FF0000);
  u_int32_t roi_pix;
  if (saving)
    roi_pix = htonl(0xFF000080);
  else
    roi_pix = htonl(0x0000FF40);
  int x,y,o=0;
  if (rect.valid) {
    for (y=0; y<rect.y; y++) {
      for(x=0; x<display.width; x++) {
        mask[o++] = roi_pix;
      }
    }
    for (y=rect.y; y<rect.y+rect.height; y++) {
      for(x=0; x<rect.x; x++) {
        mask[o++] = roi_pix;
      }
      for(x=rect.x; x<rect.x+rect.width; x++) {
        mask[o++] = none_pix;
      }
      for(x=rect.x+rect.width; x<display.width; x++) {
        mask[o++] = roi_pix;
      }
    }
    for (y=rect.y+rect.height; y<display.height; y++) {
      for(x=0; x<display.width; x++) {
        mask[o++] = roi_pix;
      }
    }
  } else {// roi not active
    for(o=0; o<display.height*display.width; o++) {
      mask[o] = none_pix;
    }
  }
  if (sensor.valid) {
    for (y=sensor.y; y<sensor.y+sensor.height; y++) {
      o = y*display.width + sensor.x;
      for(x=0; x<sensor.width; x++) {
        mask[o++] |= sensor_pix;
      }
    }
  }
}

void *overlay_task(void *fifo_name) {
  FILE *bmovlfifo;
  u_int32_t* mask = NULL;
  mask = (u_int32_t*) g_malloc(display.width*display.height*sizeof(u_int32_t));
  (bmovlfifo = fopen((char*)fifo_name, "w")) NN_OR_DIE("fopen(bmovlfifo)");
  fprintf(bmovlfifo, "ALPHA 0 0 0 0 0\n");
  while (1) {
    gboolean quit;
    struct ROI maskRegion;

    pthread_mutex_lock(&update_overlay_mutex);
    do {
      pthread_cond_wait(&overlay_update, &update_overlay_mutex);
      // where we awakened to quit ?
      pthread_mutex_lock(&end_of_world_mutex);
      quit = end_of_world;
      pthread_mutex_unlock(&end_of_world_mutex);
    } while (!quit && update_overlay == false);

    maskRegion = subRegion;
    update_overlay = false;
    pthread_mutex_unlock(&update_overlay_mutex);

    if (quit) break;

    gboolean saving;
    pthread_mutex_lock(&saving_state_mutex);
    if (saving_state == SAVE_NO)
      saving = false;
    else
      saving = true;
    pthread_mutex_unlock(&saving_state_mutex);

    generate_mask(mask, maskRegion, saving);

    fprintf(stderr, "\nsubRegion: +%d +%d %dx%d\n",
            subRegion.x, subRegion.y, subRegion.width, subRegion.height);

    fprintf(bmovlfifo, "RGBA32 %d %d 0 0 0 1\n",
            display.width, display.height) NM1_OR_DIE("overlay1");
#pragma GCC diagnostic ignored "-Wunused-value"
    if (fwrite(mask, 4, display.width*display.height, bmovlfifo)
        < (size_t)display.width*display.height) DIE("overlay2");
#pragma GCC diagnostic pop
    fflush(bmovlfifo);
  }
  g_free(mask);
  fclose(bmovlfifo);
  fprintf(stderr, "overlay_task: quitting as requested\n");
  pthread_exit(NULL);
}

void *videostream_task(void *fifo) {
  FILE *out = (FILE*) fifo;
  while (1) {
    const uint8_t *buffer;
    gboolean quit;
    pthread_mutex_lock(&pixel_data_mutex);
    do {
      pthread_cond_wait(&videostream_update, &pixel_data_mutex);
      // where we awakened to quit ?
      pthread_mutex_lock(&end_of_world_mutex);
      quit = end_of_world;
      pthread_mutex_unlock(&end_of_world_mutex);
    } while (!quit && pixel_data == NULL);

    buffer = pixel_data;
    pixel_data = NULL;
    // we know the pixel buffers are never de-allocated, so we may
    // unlock the pointer in order not to block the acquisition thread
    // for too long: at worst the pixel data 'buffer' points at will
    // be corrupted by newer data - it's just the display; ideally we
    // would want a reentrant buffer-requeueing function for the
    // acquisition, so that we may keep the lock until we're done
    // without delaying everything...
    pthread_mutex_unlock(&pixel_data_mutex);
    if (quit) break;
    pthread_cond_signal(&overlay_update);

    // stream data to mplayer
    uint8_t padding[display.width];// could initialize, but don't care
    int padding_right = display.width - imageROI.width;
    int padding_bottom = display.height - imageROI.height;
#pragma GCC diagnostic ignored "-Wunused-value"
    for (int y=0; y<imageROI.height; y++) {
      if (fwrite(buffer+y*imageROI.width, 1, imageROI.width, out)
    < (size_t)imageROI.width) DIE("streamer");
      if (padding_right > 0 && fwrite(padding, 1, padding_right, out)
    < (size_t)padding_right) DIE("streamer");
    }
    for (int y=0; y<padding_bottom; y++) {
      if (fwrite(padding, 1, display.width, out)
    < (size_t)display.width) DIE("streamer");
    }
#pragma GCC diagnostic pop
    fflush(out) Z_OR_DIE("fflush");
  }
  pthread_exit(NULL);
}

static void
set_cancel (int signal)
{
  end_world();
}

static void
control_lost_cb (ArvGvDevice *gv_device)
{
  fprintf (stderr,"Control lost\n");
  end_world();
}

int main(int argc, char* argv[])
{
  ApplicationData data;
  void (*old_sigint_handler)(int);
  GOptionContext *context;
  GError *error = NULL;
  unsigned int n_devices, i;

  g_print("This is gevCapture version %s\n",VERSION);

  data.buffer_count = 0;
  data.saved_count = 0;

  //g_type_init(); // depreciated, but required on older installs

  /* Parse command line arguments */

  context = g_option_context_new (NULL);
  g_option_context_add_main_entries (context, arv_option_entries, NULL);

  if (!g_option_context_parse (context, &argc, &argv, &error)) {
    g_option_context_free (context);
    g_print ("Option parsing failed: %s\n", error->message);
    g_error_free (error);
    return EXIT_FAILURE;
  }

  g_option_context_free (context);

  continuous_acquisition = (arv_option_trigger == NULL &&
                            arv_option_software_trigger_timeout <= 0.0);

  open_output_files(arv_option_file_root);

  arv_debug_enable (arv_option_debug_domains);

  arv_enable_interface ("Fake");
  arv_update_device_list();
  n_devices = arv_get_n_devices ();
  if (n_devices <= 0)
    printf("Aravis did not find any cameras...\n");
  else
    printf("Aravis found %d camera%s\n", n_devices, n_devices>1?"s:":":");
  for (i = 0; i < n_devices; i++) {
    printf("Device #%d: %s\n", i, arv_get_device_id (i));
  }

  signal(SIGPIPE, SIG_IGN); // ignore SIGPIPE throughout this program
  old_sigint_handler = signal (SIGINT, set_cancel);

  /*
   * find and initialise the camera
   */

  imageROI = start_camera();

  fprintf(stderr, "ROI: %d %d %d %d\n",
          imageROI.x, imageROI.y, imageROI.width, imageROI.height);
  // to work around limitations of bmovl and mplayer, our display
  // dimensions need to be multiples of 32...
  const u_int32_t roundup = 0x1F;
  display.width = (imageROI.width+roundup) & ~roundup;
  display.height = (imageROI.height+roundup) & ~roundup;

  // initialize subregion
  if (arv_option_save_x_offset >= 0)
    subRegion.x = arv_option_save_x_offset;
  else
    subRegion.x = 0;
  if (arv_option_save_y_offset >= 0)
    subRegion.y = arv_option_save_y_offset;
  else
    subRegion.y = 0;
  if (arv_option_save_width >= 0)
    subRegion.width = arv_option_save_width;
  else
    subRegion.width = imageROI.width;
  if (arv_option_save_height >= 0)
    subRegion.height = arv_option_save_height;
  else
    subRegion.height = imageROI.height;
  subRegion.valid = false;
  // initialize Sensor to something
  sensor.x = imageROI.width/3;
  sensor.y = imageROI.height/3;
  sensor.width = imageROI.width/3;
  sensor.height = imageROI.height/3;
  sensor.valid = false;

  /*
   * create FIFOs
   */
  char tempdir[] = "/tmp/gevCaptureXXXXXX";
  const char* streamfifo_basename = "stream";
  const char* bmovlfifo_basename = "bmovl";
  char streamfifo_name[strlen(tempdir)+strlen(streamfifo_basename)+2];
  char bmovlfifo_name[strlen(tempdir)+strlen(bmovlfifo_basename)+2];
  FILE *streamfifo;

  // create temporary directory
  mkdtemp(tempdir) NN_OR_DIE("mkdtemp");

  // create stream fifo
  for (unsigned int i=0; i<sizeof(streamfifo_name); i++)
    streamfifo_name[i] = '\0';
  strcpy(streamfifo_name, tempdir);
  streamfifo_name[strlen(tempdir)] = '/';
  strcpy(streamfifo_name+strlen(tempdir)+1, streamfifo_basename);
  printf("stream fifo: %s\n",streamfifo_name);
  mkfifo(streamfifo_name, 0644) Z_OR_DIE("creating stream fifo");

  // create bmovl fifo
  for (unsigned int i=0; i<sizeof(bmovlfifo_name); i++)
    bmovlfifo_name[i] = '\0';
  strcpy(bmovlfifo_name, tempdir);
  bmovlfifo_name[strlen(tempdir)] = '/';
  strcpy(bmovlfifo_name+strlen(tempdir)+1, bmovlfifo_basename);
  printf("bmovl fifo: %s\n",bmovlfifo_name);
  mkfifo(bmovlfifo_name, 0644) Z_OR_DIE("creating bmovl fifo");

  /*
   * spawn mplayer
   */

  //create pipes to communicate with mplayer
  int pipe1[2], pipe2[2], to_mplayer, from_mplayer;
  pipe(pipe1) Z_OR_DIE("pipe");
  pipe(pipe2) Z_OR_DIE("pipe");

  // fork and exec
  pid_t pid;
  (pid = fork()) NM1_OR_DIE("fork");
  if (pid == 0) { // child becomes mplayer

    // redirect standard in/out
    dup2(pipe1[0], STDIN_FILENO) NM1_OR_DIE("child fork stdin redirect");
    dup2(pipe2[1], STDERR_FILENO) NM1_OR_DIE("child fork stdout redirect");
    // close filedescriptors we do not need
    close(pipe1[0]) Z_OR_DIE("child fork close p1 read end");
    close(pipe1[1]) Z_OR_DIE("child fork close p1 write end");
    close(pipe2[0]) Z_OR_DIE("child fork close p2 read end");
    close(pipe2[1]) Z_OR_DIE("child fork close p2 write end");

    // build argument list
    const char *argv[MPLAYER_MAXARGC];
    argc = 0;
    argv[argc++] = "mplayer";
    argv[argc++] = "-slave";
    argv[argc++] = "-quiet";
    argv[argc++] = "-input";
    argv[argc++] = "nodefault-bindings:conf=/dev/null";
    argv[argc++] = "-demuxer";
    argv[argc++] = "rawvideo";
    argv[argc++] = "-rawvideo";
    const int MPparams_rawvideo = argc++;
    //argv[8] = "fps=25:w=510:h=500:y8";
    argv[argc++] = "-vf";
    const int MPparams_bmovl = argc++;
    const int MPparams_bmovlfifo = argc++;
    //argv[10] = "bmovl=0:1:%s";
    //argv[11] = "%s";
    argv[argc++] = "-sws";
    argv[argc++] = "4";
    //argv[argc++] = "-vf";
    //argv[argc++] = "scale=1024:-3";
    argv[argc++] = "-vo";
    argv[argc++] = "gl";
    if (arv_option_window_fit) {
      argv[argc++] = "-fs"; // hijack this option for fullscreen
      //argv[argc++] = "-x";
      //argv[argc++] = "1200";
      //argv[argc++] = "-y";
      //argv[argc++] = "960";
      argv[argc++] = NULL;
    } else {
      argv[argc++] = NULL;
    }
#pragma GCC diagnostic ignored "-Wunused-value"
    if (argc >= MPLAYER_MAXARGC) DIE("assertion argc < MPLAYER_MAXARGC failed");
#pragma GCC diagnostic pop

    // some arguments have to be hand-crafted:
    char buf1[BUFSIZE], buf2[BUFSIZE];
    if ((unsigned int)snprintf(buf1, sizeof(buf1), "fps=%f:w=%d:h=%d:y8",
                               display_fps_default, display.width, display.height) >= sizeof(buf1)) {
      fprintf(stderr, "buf1 not large enough for arg !\n");
      exit(EXIT_FAILURE);
    }
    argv[MPparams_rawvideo] = buf1;
    if ((unsigned int)snprintf(buf2, sizeof(buf2), "bmovl=0:1:%s",
                               bmovlfifo_name) >= sizeof(buf2)) {
      fprintf(stderr, "buf2 not large enough for arg !\n");
      exit(EXIT_FAILURE);
    }
    argv[MPparams_bmovl] = buf2;
    argv[MPparams_bmovlfifo] = streamfifo_name;

    // become mplayer
    execvp(argv[0],(char* const*) argv);
#pragma GCC diagnostic ignored "-Wunused-value"
    DIE("execvp mplayer failed"); // this should never be reached
#pragma GCC diagnostic pop
  }

  // if we're here we're the parent
  // take care of pipe matters
  close(pipe1[0]) Z_OR_DIE("child fork close p1 read end");
  close(pipe2[1]) Z_OR_DIE("child fork close p2 write end");
  long flags;
  (flags = fcntl(pipe1[1], F_GETFD)) NM1_OR_DIE("fcntl getfd");
  flags |= FD_CLOEXEC;
  fcntl(pipe1[1], F_SETFD, flags) NM1_OR_DIE("fcntl setfd");
  (flags = fcntl(pipe2[0], F_GETFD)) NM1_OR_DIE("fcntl getfd");
  flags |= FD_CLOEXEC;
  fcntl(pipe2[0], F_SETFD, flags) NM1_OR_DIE("fcntl setfd");
  // only need to remember these two file descriptors:
  to_mplayer = pipe1[1];
  from_mplayer = pipe2[0];
  FILE *tomplayer;
  (tomplayer = fdopen(to_mplayer, "w")) NN_OR_DIE("fdopen to_mplayer");
  FILE *frommplayer;
  (frommplayer = fdopen(from_mplayer, "r")) NN_OR_DIE("fdopen from_mplayer");

  /*
   * fork user interface threads
   */

  // make stdin unbuffered (switch off canonical mode)
  static struct termios oldt, newt;
  // tcgetattr gets the parameters of the current terminal
  tcgetattr( STDIN_FILENO, &oldt);
  newt = oldt;
  // with ICANON, stdio functions normally see a line of input only after
  // a "\n" or an EOF or an EOL has been encountered; switch off
  newt.c_lflag &= ~(ICANON);
  // set new attributes; TCSANOW tells tcsetattr to change attr. immediately.
  tcsetattr( STDIN_FILENO, TCSANOW, &newt);

  pthread_t err_thread, stdin_thread, videostream_thread, overlay_thread, buffer_save_thread;

  pthread_create(&err_thread, NULL, err_listener, frommplayer) Z_OR_DIE("pthread_create");
  pthread_create(&stdin_thread, NULL, stdin_listener, stdin) Z_OR_DIE("pthread_create");

  // We use two FIFOs to exchange data with mplayer (video stream and
  // bmovl overlay mask). If we do not open them in the same order as
  // mplayer, we have a deadlock. We have four options: 1) trust that
  // mplayer will always open the FIFOs in the same order, and do
  // likewise (streamfifo, then bmovlfifo). 2) put the FIFOs in
  // non-blocking mode (but affects all subsequent read/write
  // operations. 3) spawn a thread for one of the FIFOs. At present we
  // chose option 1 -- edit: looks more like option 4 in the meantime:
  // one thread for each FIFO).

  (streamfifo = fopen(streamfifo_name, "w")) NN_OR_DIE("fopen(streamfifo)");
  // output dummy frame, mplayer waits for at least one byte on
  // streamfifo before proceeding with further initialization.
  dummyframe(streamfifo);

  // this thread streams video data to mplayer
  pthread_create(&videostream_thread, NULL, videostream_task, streamfifo) Z_OR_DIE("pthread_create");

  // this thread updates the overlay mask
  pthread_create(&overlay_thread, NULL, overlay_task, bmovlfifo_name) Z_OR_DIE("pthread_create(bmovl)");

  // this thread saves buffers to disk
  pthread_create(&buffer_save_thread, NULL, buffer_save_task, NULL) Z_OR_DIE("pthread_create(buffer_save)");

  /*
   *   capture loop
   */

  clock_getres(CLOCK_MONOTONIC, &clock_res);
  clock_gettime(CLOCK_MONOTONIC, &clock_now);
  fprintf(stderr, "timer resolution: %ld.%06ld\n",
          clock_res.tv_sec, clock_res.tv_nsec);
  fprintf(stderr, "time now: %ld.06%ld\n",
          clock_now.tv_sec, clock_now.tv_nsec);

  clock_gettime(CLOCK_MONOTONIC, &clock_origin);
  clock_last.tv_sec = 0;
  clock_last.tv_nsec = 0;

  g_signal_connect (stream, "new-buffer", G_CALLBACK (new_buffer_cb), &data);
  arv_stream_set_emit_signals (stream, true);

  g_signal_connect (arv_camera_get_device (camera), "control-lost",
                    G_CALLBACK (control_lost_cb), NULL);

  // this callback just periodically checks if end_of_world has been
  // set in one of the threads
  g_timeout_add_seconds (1, periodic_task_cb, &data);

  /* start grabbing */
  arv_camera_start_acquisition (camera);

  /* Now that gevCapture and the camera are ready for acquisition,
     activate the software trigger timer. */
  if (!continuous_acquisition) {
    start_stop_saving();
    if (arv_option_software_trigger_timeout > 0.0) {
      arv_camera_set_trigger (camera, "Software");
      software_trigger_source = g_timeout_add ((guint) (arv_option_software_trigger_timeout * 1000.0),
                                               emit_software_trigger, camera);
    }
  }

  data.main_loop = g_main_loop_new (NULL, false);

  g_main_loop_run (data.main_loop);

  /*
   *   Stop and clean up stuff
   */

  if (software_trigger_source > 0)
    g_source_remove (software_trigger_source);

  signal (SIGINT, old_sigint_handler);

  g_main_loop_unref (data.main_loop);

  stop_camera();
  g_object_unref (stream);

  /*
   * quit mplayer and helper threads
   */

  end_world();

  pthread_cond_signal(&videostream_update); // last wake-up call
  pthread_join(videostream_thread, NULL) Z_OR_DIE("pthread_join");

  pthread_cond_signal(&buffer_save_new);
  pthread_join(buffer_save_thread, NULL) Z_OR_DIE("pthread_join");

  // close files
  fclose(stampFile);
  stampFile = NULL;

  fprintf(stderr,"sending mplayer quit command ...");
  fprintf(tomplayer,"quit\n");
  fflush(tomplayer);
  dummyframe(streamfifo);// in case mplayer still waits for an incomplete frame
  dummyframe(streamfifo);
  fprintf(stderr,"done\n");

  /* these threads should terminate as quitting mplayer breaks their pipes */
  pthread_join(err_thread, NULL) Z_OR_DIE("pthread_join");
  pthread_mutex_lock(&update_overlay_mutex);
  update_overlay = true;
  pthread_mutex_unlock(&update_overlay_mutex);
  pthread_cond_signal(&overlay_update);
  pthread_join(overlay_thread, NULL) Z_OR_DIE("pthread_join");

  /* this is probably stuck on fgetc, but there is no specific
   * clean-up to do anyway, so ... */
  pthread_cancel(stdin_thread) Z_OR_DIE("pthread_join");

  /* restore the old terminal settings */
  tcsetattr( STDIN_FILENO, TCSANOW, &oldt);

  pthread_mutex_destroy(&pixel_data_mutex) Z_OR_DIE("destroy mutex");
  pthread_mutex_destroy(&saving_state_mutex) Z_OR_DIE("destroy mutex");
  pthread_mutex_destroy(&update_overlay_mutex) Z_OR_DIE("destroy mutex");
  pthread_mutex_destroy(&end_of_world_mutex) Z_OR_DIE("destroy mutex");
  pthread_mutex_destroy(&buffer_save_mutex) Z_OR_DIE("destroy mutex");
  pthread_cond_destroy(&videostream_update);
  pthread_cond_destroy(&overlay_update);
  pthread_cond_destroy(&buffer_save_new);

  close(to_mplayer) Z_OR_DIE("pclose to_mplayer");
  close(from_mplayer) Z_OR_DIE("pclose from_mplayer");
  fclose(streamfifo) Z_OR_DIE("fclose(streamfifo)");

  unlink(streamfifo_name) Z_OR_DIE("removing stream fifo");
  unlink(bmovlfifo_name) Z_OR_DIE("removing bmovl fifo");
  rmdir(tempdir) Z_OR_DIE("removing temporary directory");

  fprintf(stderr,"*** the end ***\n");

  return EXIT_SUCCESS;
}
